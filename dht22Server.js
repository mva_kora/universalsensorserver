//require======================================
var http = require('http');
var express = require('express');
var formidable = require("formidable");
var bodyParser = require("body-parser");
var mustacheExpress = require('mustache-express');
var WebSocketServer = require('ws').Server;
var sensorLib = require('node-dht-sensor');

//DHT22 Sensor==================
var humValue, tempValue;	//Variablen zur speicherung der Sensordaten

var sensor = {				//Anlegen der Sensorklasse
	initialize: function () {
		return sensorLib.initialize(22, 4);
	},
	read: function () {
		var readout = sensorLib.read();
		tempValue = readout.temperature.toFixed(2);
		humValue = readout.humidity.toFixed(2);
	}
};

if (sensor.initialize()) {	//Initialisierung des Sensors
    console.log('Sensor initialized\n');
} else {
    console.warn('Failed to initialize sensor');
}

//Websocket Server=============================

var clients = [ ];		//Liste aller verbundenen Websocket-Clients
var date_list = [];		//Verbindungszeiten der einzelnen Clients

var wss = new WebSocketServer({port: 81});	//Anlegen des Websocket Servers
console.log("Websocket-Server listening on Port: 81");
wss.on('connection', function(ws) {			//Aufbau der Verbindung
    ws.on('message', function(message) {	//Abfragen der message
        if ( message == 'request_data' )
		{
			clients.push(ws);				//Bei richtiger message ablegen des ws in die clients[] Liste
			date_list.push(new Date());		
			console.log("Client "+ws.upgradeReq.connection.remoteAddress+" connected. ("+new Date()+")");	
		}
		else
		{
			ws.close(1000, 'Wrong message during connection');	C//Close bei falscher message
		}
    });
    ws.on('close', function close() {				//Close Anfrage des Clients
		for (var i = 0; i < clients.length; i++)
		{
			if( clients[i] == ws )
			{
				clients.splice(i, 1);				//löschen des ws aus der clients[] Liste
				date_list.splice(i, 1);
				console.log("Client "+ws.upgradeReq.connection.remoteAddress+" disconnected. ("+new Date()+")");
			}
		}
	});
		
});

function intervalhum() {		//Auslesen der Feuchtigkeit und senden an alle verbundenen Clients 
	sensor.read();				//Sensor auslesen
	if ( clients.length > 0 ) 	//Wenn Clients vorhanden sind
	{
		for (var i = 0; i < clients.length; i++) //Für alle Clients
		{
			try 
			{
				if (clients[i]) clients[i].send("HUM?"+humValue); //Sende Sensordaten
			} 
			catch (e)
			{
				console.log('Error HUM: '+e);			
			}
				
		}
	}
	setTimeout( intervalhum, _intervalHum );	//Rekursiver Aufruf der sendefunktion alle _intervalHum ms
}

function intervaltemp() {		//Wie intervalhum jedoch für die Temperatur
	sensor.read();
	if ( clients.length > 0 ) 
	{
		for (var i = 0; i < clients.length; i++) 
		{
			try 
			{
				if (clients[i]) clients[i].send("TEMP?"+tempValue);
			} 
			catch (e)
			{
				console.log('Error Temp: '+e);
			}
		}
	}
	setTimeout( intervaltemp, _intervalTemp );
}

//express HTTP-Server==========================
var app = express();

app.engine('html', mustacheExpress());          // register file extension mustache
app.set('view engine', 'html');                 // register file extension for partials
app.set('views', __dirname + '/html');
app.use(express.static(__dirname + '/public')); // set static folder
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const PORT = 80; 			//PORT des HTTP Servers

var _intervalTemp = 500; 	//Abfrageinterval des Temperatursensors in Milisekunden
var _intervalHum = 500;		//Abfrageinterval des Temperatursensors in Milisekunden

app.get('/', function(request, response){
	sensor.read();
	var ip_list = [];
	if( clients.length > 0 )
	{
		for (var i = 0; i < clients.length; i++)
		{
			if (clients[i]) ip_list.push(clients[i].upgradeReq.connection.remoteAddress);
		}
		response.render('dht22Server', {hum : humValue, temp : tempValue, intervalHum : _intervalHum, intervalTemp : _intervalTemp, client_list : ip_list, date_list : date_list});
	}
	else
	{
	response.render('dht22Server', {hum : humValue, temp : tempValue, intervalHum : _intervalHum, intervalTemp : _intervalTemp, client_list : 'no clients connected', date_list : ''});
	}
});

app.post('/', function(request, response){
	sensor.read();
	if ( request.body.intervalHum ) {
		_intervalHum = request.body.intervalHum;
		console.log("Helligkeitsintervall geaendert: ", request.body.intervalHum);
		console.log("lumValue : %d",humValue);
	}
	if ( request.body.intervalTemp ) {
		_intervalTemp = request.body.intervalTemp;
		console.log("Temperaturintervall geaendert: ", request.body.intervalTemp);
		console.log("tempValue : %d",tempValue);
	}
	var ip_list = [];
	if( clients.length > 0 )
	{
		for (var i = 0; i < clients.length; i++)
		{
			if (clients[i]) ip_list.push(clients[i].upgradeReq.connection.remoteAddress);
		}
		response.render('dht22Server', {hum : humValue, temp : tempValue, intervalHum : _intervalHum, intervalTemp : _intervalTemp, client_list : ip_list, date_list : date_list});
	}
	else
	{
	response.render('dht22Server', {hum : humValue, temp : tempValue, intervalHum : _intervalHum, intervalTemp : _intervalTemp, client_list : 'no clients connected', date_list : ''});
	}
});

//sendeaufruf per websocket zur App=============
setTimeout( intervalhum, _intervalHum );
setTimeout( intervaltemp, _intervalTemp );

app.listen(PORT, function(){console.log("HTTP-Server listening on Port:%s\n", PORT)}); 


