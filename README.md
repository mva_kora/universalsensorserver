# README #



### Kurzbeschreibung ###

Dieses Projekt implementiert einen in node.js geschriebenen HTTP- und Websocketserver zum senden von Sensordaten. Der Server wurde auf einem Raspberry Pi entwickelt und getestet. Als Sensor wurde ein Temperatur- und Luftfeuchtigkeitssensor vom Typ DHT22 verwendet. In der folgenden Abbildung ist die Verdrahtung am Raspberry Pi 1 Model B zu sehen.
 
![Anschlussplan des DHT22](https://www.sweetpi.de/blog/wp-content/uploads/2013/11/dht22_Steckplatine2.png "Anschlussplan des DHT22")

Als Client diente eine Universal Windows Plattform App welche unter [Webdav UWP CLient](https://bitbucket.org/mva_kora/universalsensorclient) veröffentlicht wurde.

### Installation ###

Voraussetzung für das Projekt ist ein Raspberry Pi mit installiertem Raspbian Jessie. 
Vor der Installation von node.js sollte noch Update des Systems mittels der folgenden Befehle durchgeführt werden:

```
#!shell

sudo apt-get update
sudo apt-get dist-upgrade
sudo rpi-update
sudo reboot
```

Anschließend kann die Installation von node.js erfolgen. 
Dafür müssen die folgenden Befehle ausgeführt werden (Raspberry Pi Model A,B,B+):

```
#!shell

wget https://nodejs.org/dist/v4.0.0/node-v4.0.0-linux-armv6l.tar.gz
tar -xvf node-v4.0.0-linux-armv6l.tar.gz
cd node-v4.0.0-linux-armv6l
sudo cp -R * /usr/local/
```

Für den Raspberry Pi 2 Model B entsprechend:

```
#!shell

wget https://nodejs.org/dist/v4.0.0/node-v4.0.0-linux-armv7l.tar.gz
tar -xvf node-v4.0.0-linux-armv7l.tar.gz
cd node-v4.0.0-linux-armv7l
sudo cp -R * /usr/local/
```

Eine Überprüfung der Version kann mittels 'node -v' erfolgen. 

Das Repository kann über die folgenden git-Befehle (oder als .zip unter Downloads) heruntergeladen werden:

```
#!shell

mkdir ~/universalSensorServer
cd ~/universalSensorServer
git init
git clone https://bitbucket.org/mva_kora/universalsensorserver
```

Der Server kann nun über den Befehl `sudo node dht22Server.js` gestartet werden. 
Standardmäßig ist der Websocket-Server über Port 81 und der HTTP-Server über Port 80 erreichbar.